const DarkColor = '#122237';
const MidColor = '#2f67a8';
const LightColor = '#cfd2d7';

export { DarkColor, MidColor, LightColor };