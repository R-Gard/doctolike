import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import { MeetScreen } from "../screens/Index";
import DoctorDetail from "../screens/DoctorDetail";

const HomeStack = createStackNavigator({
  Home: {
    screen: MeetScreen
  }, 
  DoctorDetail: {
    screen: DoctorDetail 
  }
});

export default createAppContainer(HomeStack);