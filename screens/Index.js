import React from "react";
import Screen from "./Screen";
import Home from "./Home";

export const MeetScreen = ({navigation}) => <Screen navigation={navigation} name="Rendez-vous" screen={Home}/>
export const DocumentScreen = ({navigation}) => <Screen navigation={navigation} name="Mes documents"/>
export const AccountScreen = ({navigation}) => <Screen navigation={navigation} name="Mon compte"/>
export const RelationScreen = ({navigation}) => <Screen navigation={navigation} name="Mes proches"/>
