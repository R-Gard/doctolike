import React from "react";
import DoctorList from "../components/DoctorList";

const Home = (props) => {
  console.log(props)
  const pressHandler = () => {
    props.navigation.navigate("DoctorDetail");
  }
  return (
      <DoctorList navigation={props.navigation}/>
  )
};

export default Home;