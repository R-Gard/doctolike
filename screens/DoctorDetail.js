import React from "react";
import { StyleSheet, Text, View } from "react-native";

const DoctorDetail = ({ navigation}) => {
  return (
    <View>
      <Text>Doctor Details</Text>
      <Text>{ navigation.getParam('item').name }</Text>
      <Text>{ navigation.getParam('item').role }</Text>
      <Text>{ navigation.getParam('item').city }</Text>
    </View>
  )
};

export default DoctorDetail;