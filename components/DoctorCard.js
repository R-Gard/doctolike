import React from "react";
import { View, Text, StyleSheet } from "react-native";
import ThumbProfil from "../components/ThumbProfil";

const DoctorCard = ( props ) => {
  const { image, name, role, city} = props.doctor;
  return (
    <View style={styles.container}>
      <ThumbProfil src={image} />
      <View style={styles.info}>
        <Text style={styles.doctor}>{name}</Text>
        <Text>{role}</Text>
        <Text>{city}</Text>
      </View>
    </View>
  )
};

export default DoctorCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#d8d8d8",
    padding: 10,
    width: "90%"
  },
  info: {
    marginLeft: 20,
  },
  doctor: {
    color: '#2f67a8',
    fontWeight: "bold",
    fontSize: 18
  }
});