import React from "react";
import { View, Text, StyleSheet, ScrollView, ImageBackground, Image, Button } from "react-native";
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import ThumbProfil from "./ThumbProfil";
const SideBar = props => (
  <ScrollView>
    <ImageBackground
      source={require('../assets/images/banner.png')} 
      style={{width: undefined, padding:16}}
    >
      <View style={styles.info}>
        {/* <Image 
          source={require('../assets/images/r.jpg')} 
          style={styles.profile}
        /> */}
        <ThumbProfil src={require('../assets/images/r.jpg')} />
        <Text style={styles.name}>Rudy F</Text>
      </View>
    </ImageBackground>

    <View style={styles.container}>
      <DrawerNavigatorItems {...props} />
    </View>

    <View> 
      <Text style={styles.version}>V.4.0.4</Text>
    </View>
    
    {/* 
    <View style={styles.logout}>
      <Button
        title="Se deconnecter"
        // onPress={() => navigation.navigate('Login')}
      />
    </View> 
    */}
  </ScrollView>
);
export default SideBar;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  // profile: {
  //   width: 80,
  //   height: 80,
  //   borderRadius: 40,
  //   borderWidth: 3,
  //   borderColor: "#FFF"
  // },
  name: {
    color: "#FFF",
    fontSize: 20,
    fontWeight: "800",
    marginHorizontal: 15,
    marginVertical: 25
  }, 
  info: {
    flexDirection: "row",
  },
  version: {
    color: "lightgrey",
    marginLeft: 20,
  },
  logout: {
    // width: 100,
    // alignSelf: "end"
  }

});