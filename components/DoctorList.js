import React from "react";
import { StyleSheet, FlatList, SafeAreaView, TouchableOpacity } from "react-native";
import DoctorCard from "../components/DoctorCard";
import DataDoctor from '../data/doctor';

const DoctorList = (props) => {

  return (
    <SafeAreaView style={styles.container}>
      <FlatList 
        data={DataDoctor}
        renderItem= { ({item}) => (
          <TouchableOpacity onPress={()=>props.navigation.navigate('DoctorDetail', {item})}>
            <DoctorCard doctor={item}/>  
          </TouchableOpacity> 
        )}
        keyExtractor={item => item.id}
        // keyExtractor={(item, index) => item.key}
      />
    </SafeAreaView>
  )
};

export default DoctorList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%"
  },
});