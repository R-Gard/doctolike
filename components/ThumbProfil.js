import React from 'react';
import {StyleSheet, Image } from "react-native";

const ThumbProfil = ({ src }) => {
  // const imgSrc = require('../assets/images/banner.png');

  return (
    <Image 
      source={src} 
      style={styles.profile}
    />
  )
};

export default ThumbProfil;

const styles = StyleSheet.create({
  profile: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: "#FFF"
  }
});