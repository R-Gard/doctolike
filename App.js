import React from "react";
import { createAppContainer } from "react-navigation";
import { createDrawerNavigator} from "react-navigation-drawer";
import { Dimensions } from "react-native";
import { Feather } from "@expo/vector-icons";
// import { DarkColor, MidColor, LightColor } from "./constants/Colors";

import { 
  // MeetScreen,
  DocumentScreen,
  AccountScreen,
  RelationScreen
} from "./screens/Index";

import HomeStack from "./routes/homeStack";

import SideBar from './components/SideBar';

const DrawerNavigator = createDrawerNavigator({
  MeetScreen: {
    screen: HomeStack,
    navigationOptions: {
      title: "Mes rendez-vous",
      drawerIcon: () => <Feather name="calendar" size={16} color={"#fff"} />
    },
  },
  DocumentScreen: {
    screen: DocumentScreen,
    navigationOptions: {
      title: "Mes documents",
      drawerIcon: () => <Feather name="file" size={16} color={"#fff"} />
    }
  },
  AccountScreen: {
    screen: AccountScreen,
    navigationOptions: {
      title: "Mon compte",
      drawerIcon: () => <Feather name="user" size={16} color={"#fff"} />
    }
  },
  RelationScreen: {
    screen: RelationScreen,
    navigationOptions: {
      title: "Mes proches",
      drawerIcon: () => <Feather name="users" size={16} color={"#fff"} />
    }
  },
}, {
  contentComponent : props => <SideBar {...props} />,
  drawerPosition : "right",
  drawerWidth: Dimensions.get("window").width *0.85,
  drawerBackgroundColor: "#122237",
  hideStatusBar: true,
  contentOptions: {
    activeBackgroundColor: "#2f67a8",
    activeTintColor: "#fff",
    inactiveTintColor: "#fff",
    itemContainerStyle: {
      marginTop: 16,
      marginHorizontal: 8
    },
    itemStyle: {
      borderRadius: 4
    }
  }

});

export default createAppContainer(DrawerNavigator);